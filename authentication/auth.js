const jwt = require("jsonwebtoken");
const Customer = require("../models/model.js");
require("dotenv").config();

const auth = async (req, res, next) => { 
    try {
        const token = req.cookies.login;     
        const verifyUser = jwt.verify(token, process.env.SECRET_KEY);
        const user = await Customer.findOne({ _id: verifyUser._id });
        console.log(user);
        if (user) {
            next();
        }
        else {
            res.status(401).send("please login in");
        }
        
    } catch (error) {
        res.status(401).send("please login in");
    }
}

module.exports = auth;