const express = require("express");
require("../connection/connect.js");
const router = express.Router();
const customer = require("../models/model.js");
const auth = require("../authentication/auth.js");
const cookieParser = require("cookie-parser");


router.use(express.json());
router.use(cookieParser());



router.get("/", async (req, res) => {
    try {
        res.send("homepage")
    } catch (error) {
        res.send(error);
    }
});
router.get("/login", async (req, res) => {
try {
    const user = req.body.email;
    const pass = req.body.password;
    const userSearch = await customer.findOne({ username: user, password: pass });
    if (userSearch) {
        
        //generate token
        const token = await userSearch.generateAuthToken();
        // res.status(200).send("login in successful");

        res.cookie("login", token, {
			expires: new Date(Date.now() + 10000),
			httpOnly: true,
        });
        res.status(200).send("cookies created successfully");
    }
    else { 
        res.send("user not found");
    }
} catch (error) {
    res.status(401).send(error);
}
});

router.post("/customer", async (req, res) => {
    try {
        const newCustomer = customer(req.body);
        const createCustomer = await newCustomer.save();
        res.status(200).send(createCustomer);
        
    } catch (error) {
        res.status(404).send(error);
    }
});
 
router.get("/customers",auth, async (req, res) => {
    // try {
        
        const getCustomers = await customer.find();
        if (!getCustomers) {
            res.status(404).send("Customer not found");
        }
        else { 
            res.status(200).send(getCustomers);
        }

   /*  } catch (error) {
        res.status(400).send(error);
    } */
});

router.get("/customer/:name",auth, async (req, res)=>{
    try {
        const reqName = req.params.name;
        const reqCustomer = await customer.find({name:reqName});
        if (!reqCustomer.length) {
            res.status(404).send("Customer not Found");
        }
        else { 
            res.status(200).send(reqCustomer);
        }

    } catch (err) {
        res.status(404).send(err);
    }
});

router.patch("/customer/:id", auth,async (req,res) => {
    try {
        const givenId = req.params.id;
        const updateCustomer = await customer.findByIdAndUpdate(givenId, req.body, {
            new: true,
        });
        
        res.send(updateCustomer);
    } catch (error) {
        res.status(400).send(error);
    }
});
 

router.delete("/customer/:id", auth,async (req, res) => {
    try {
        const givenId = req.params.id;
        const deleteCustomer = await customer.findByIdAndDelete(givenId);
        if (!deleteCustomer) {
            res.status(404).send("Customer not found for Deleting");
        }
        else {
            res.send(deleteCustomer);
        }
    } catch (error) {
        res.send(error);
    }
 });


module.exports = router;