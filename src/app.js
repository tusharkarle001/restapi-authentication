const express = require("express");
const app = express();
const router = require("../route/route.js");
require("../connection/connect.js");
require("dotenv").config()

app.use(express.json());
app.use(router);


app.listen(8000, () => {
    console.log("connected to the port 8000")
});