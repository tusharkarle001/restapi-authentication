const res = require("express/lib/response");
const mongoose = require("mongoose");
require("../connection/connect.js");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const customerSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	age: {
		type: Number,
		required: true,
	},
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true,
	},
	tokens: {
		type: String,
	},
});

//create the method
customerSchema.methods.generateAuthToken = async function () {
	try {
		const token = jwt.sign(
			{
				_id: this._id.toString(),
			},
			process.env.SECRET_KEY
		);
		// console.log(token);
		this.tokens = token;
		await this.save();
		return token;
	} catch (error) {
		console.log("cannot generate token");
	}
};

const customer = new mongoose.model("Customer", customerSchema);

module.exports = customer;
